package com.ayu.gtest.javaClass;

/**
 * Created by guoyongliang on 2018/8/29.
 */
public interface Interface {

    void doSomething();
    void someElse(String str);

}
