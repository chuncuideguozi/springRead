package com.ayu.gtest.javaClass;

/**
 * Created by guoyongliang on 2018/8/29.
 */
public class SweetShop {
    public static void main(String[] args) {
        Class classType;
        System.out.println("inside main");
        try{
            classType = Class.forName("com.ayu.gtest.javaClass.Gum");

        }catch (ClassNotFoundException e){
            System.out.println("Could not fin Gum");
        }
        System.out.println("After creating Class.forName(\"Gum\")");
        classType=Candy.class;
        System.out.println("After creating Candy");
        Cookie cookie=new Cookie();
        classType=cookie.getClass();
        System.out.println("After creating Cookie");
    }
}

class Candy{
    static{
        System.out.println("Loading Candy");
    }
}
class Gum{
    static{
        System.out.println("Loading Gum");
    }
}
class Cookie{
    static{
        System.out.println("Loading Cookie");
    }
    public Cookie(){
        System.out.println("initializing Cookie");
    }
}
