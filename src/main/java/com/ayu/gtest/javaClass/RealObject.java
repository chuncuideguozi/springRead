package com.ayu.gtest.javaClass;

/**
 * Created by guoyongliang on 2018/8/29.
 */
public class RealObject implements Interface{
    public void doSomething() {
        System.out.println("doing something");
    }

    public void someElse(String str) {
        System.out.println("someElse "+str);
    }
}
