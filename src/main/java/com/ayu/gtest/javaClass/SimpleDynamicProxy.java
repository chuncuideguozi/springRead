package com.ayu.gtest.javaClass;

import java.lang.reflect.Proxy;

/**
 * Created by guoyongliang on 2018/8/29.
 */

public class SimpleDynamicProxy {

    public static void consumer(Interface inter){
        inter.doSomething();
        inter.someElse("bonobo");
    }
    public static void main(String[] args) {
        RealObject realObj=new RealObject();
        consumer(realObj);
        System.out.println("-----------------");
        Interface proxy=(Interface) Proxy.newProxyInstance(
                Interface.class.getClassLoader(),
                new Class[]{Interface.class},
                new DynamicProxyHandler(realObj));
        consumer(proxy);
    }
}

