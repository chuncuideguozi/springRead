package com.ayu.gtest.stringTest;

/**
 * Created by guoyongliang on 2018/9/3.
 */
public class DataUtil {

    /**
     * 将整数转换成字节数组
     */
    public static byte[] int2ByteArr(int i){
        byte[] bytes = new byte[4] ;
        bytes[0] = (byte)(i >> 24) ;
        bytes[1] = (byte)(i >> 16) ;
        bytes[2] = (byte)(i >> 8) ;
        bytes[3] = (byte)(i >> 0) ;
        return bytes ;
    }

    /**
     * 将字节数组转换成整数
     */
    public static int byteArr2Int(byte[] arr){
        return  (arr[0] & 0xff) << 24
                | (arr[1] & 0xff) << 16
                | (arr[2] & 0xff) << 8
                | (arr[3] & 0xff) << 0 ;
    }

    public static void printChar(char[] chars){
        if (chars==null){
            System.out.println("chars is null");
        }
        for (int i=0;i<chars.length;i++){
            System.out.print(chars[i]);
            System.out.print("  ");
            System.out.print((int)(chars[i]));
            System.out.println("");

        }
    }


    public static void printBytes(byte[] bytes){
        if (bytes==null){
            System.out.println("chars is null");
        }
        for (int i=0;i<bytes.length;i++){
            System.out.print(bytes[i]);
            System.out.print("  ");


        }
    }

}
