package com.ayu.gtest.stringTest;

/**
 * Created by guoyongliang on 2018/9/3.
 */
public class upperCaseFirstLatter {

    public static void main(String[] args) {
        String s = "miaohei";
        System.out.println(toUpper(s));
    }

    public static String toUpper(String str){
        char[] strChar = str.toCharArray();
        printChar(strChar);
        strChar[0] -= 32;
        return String.valueOf(strChar);
    }

    public static void printChar(char[] chars){
        if (chars==null){
            System.out.println("chars is null");
        }
        for (int i=0;i<chars.length;i++){
            System.out.print(chars[i]);
            System.out.print("  ");
            System.out.print((int)(chars[i]));
            System.out.println("");

        }
    }
}
