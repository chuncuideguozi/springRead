package com.ayu.controller;

/**
 * Created by guoyongliang on 2018/8/7.
 */
public class HelloWorld {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void hello(){
        System.out.println(name + " is  here !! ");
    }
}
