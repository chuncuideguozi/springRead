package com.ayu.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by guoyongliang on 2018/8/7.
 */
public class Main {

    public static void main(String[] args) {

        // 1. 创建spring的ioc容器
        ApplicationContext ctx = new ClassPathXmlApplicationContext("META-INF/applicationContext.xml");
        // 2. 从ioc容器中获取bean实例
        HelloWorld helloWorld = (HelloWorld) ctx.getBean("helloWord");
        helloWorld.setName("Tom");
        helloWorld.hello();
    }
}
